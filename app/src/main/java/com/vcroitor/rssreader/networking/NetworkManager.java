package com.vcroitor.rssreader.networking;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.vcroitor.rssreader.application.FeedApplication;

public class NetworkManager {

    public static boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) FeedApplication.getInstance().
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnected();
    }
}

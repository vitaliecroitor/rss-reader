package com.vcroitor.rssreader.database;


import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public final class FeedContract {

    public static final class Feed implements BaseColumns {
        public static final String AUTHORITY = "com.vcroitor.rssreader.provider";
        public static final String BASE_PATH = "feeds";
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
                + "/feeds";
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
                + "/feed";

        public static final String TABLE_NAME = "feeds";
        public static final String TITLE = "title";
        public static final String DESCRIPTION = "description";
        public static final String PUB_DATE = "pub_date";
        public static final String ARTICLE = "article";
        public static final String SMALL_IMAGE_URL = "small_image_url";
        public static final String BIG_IMAGE_URL = "big_image_url";
        public static final String URL = "url";

        public static String[] getColumns() {
            return new String[]{_ID, TITLE, DESCRIPTION, PUB_DATE,
                    ARTICLE, SMALL_IMAGE_URL, BIG_IMAGE_URL, URL};
        }
    }
}

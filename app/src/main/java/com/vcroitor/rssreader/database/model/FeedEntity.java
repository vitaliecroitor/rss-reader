package com.vcroitor.rssreader.database.model;

/**
 * Created by vitalie on 27/09/14.
 */
public class FeedEntity {

    public int id;
    public String title;
    public String description;
    public String url;
    public String pubDate;
    public String article;
    public String smallImageUrl;
    public String bigImageUrl;

    private FeedEntity(Builder builder) {
        title = builder.title;
        description = builder.description;
        pubDate = builder.pubDate;
        article = builder.article;
        smallImageUrl = builder.smallImageUrl;
        bigImageUrl = builder.bigImageUrl;
        url = builder.url;
    }

    public static class Builder {
        private String title;
        private String description;
        private String pubDate;
        private String article;
        private String smallImageUrl;
        private String bigImageUrl;
        private String url;

        public FeedEntity build() {
            return new FeedEntity(this);
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder pubDate(String pubDate) {
            this.pubDate = pubDate;
            return this;
        }

        public Builder article(String article) {
            this.article = article;
            return this;
        }

        public Builder smallImageUrl(String url) {
            this.smallImageUrl = url;
            return this;
        }

        public Builder bigImageUrl(String url) {
            this.bigImageUrl = url;
            return this;
        }

        public Builder url(String url) {
            this.url = url;
            return this;
        }
    }
}

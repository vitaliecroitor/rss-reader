package com.vcroitor.rssreader.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static android.provider.BaseColumns._ID;
import static com.vcroitor.rssreader.database.FeedContract.Feed.ARTICLE;
import static com.vcroitor.rssreader.database.FeedContract.Feed.BIG_IMAGE_URL;
import static com.vcroitor.rssreader.database.FeedContract.Feed.DESCRIPTION;
import static com.vcroitor.rssreader.database.FeedContract.Feed.PUB_DATE;
import static com.vcroitor.rssreader.database.FeedContract.Feed.SMALL_IMAGE_URL;
import static com.vcroitor.rssreader.database.FeedContract.Feed.TABLE_NAME;
import static com.vcroitor.rssreader.database.FeedContract.Feed.TITLE;
import static com.vcroitor.rssreader.database.FeedContract.Feed.URL;

/**
 * Created by vitalie on 27/09/14.
 */
public class FeedDatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "rss_feed.db";
    private static final int DATABASE_VERSION = 1;

    public FeedDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    private static final String CREATE_TABLE_FEEDS = "CREATE TABLE "
            + TABLE_NAME + "(" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + TITLE + " TEXT UNIQUE, "
            + DESCRIPTION + " TEXT, "
            + PUB_DATE + " INTEGER, "
            + ARTICLE + " TEXT, "
            + URL + " TEXT, "
            + SMALL_IMAGE_URL + " TEXT, "
            + BIG_IMAGE_URL + " TEXT);";

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(CREATE_TABLE_FEEDS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // drop existing tables and create new ones
        // change this behaviour if data save is required
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}

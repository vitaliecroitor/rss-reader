package com.vcroitor.rssreader.application;

import android.app.Application;

public class FeedApplication extends Application {

    private static FeedApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static FeedApplication getInstance() {
        return instance;
    }
}

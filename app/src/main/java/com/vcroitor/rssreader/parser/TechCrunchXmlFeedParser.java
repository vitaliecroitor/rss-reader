package com.vcroitor.rssreader.parser;

import com.vcroitor.rssreader.database.model.FeedEntity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TechCrunchXmlFeedParser {

    private static final String ITEM = "item";
    private static final String TITLE = "title";
    private static final String LINK = "link";
    private static final String PUB_DATE = "pubDate";
    private static final String DESCRIPTION = "description";
    private static final String MEDIA_THUMBNAIL = "media|thumbnail";
    private static final String URL = "url";

    public List<FeedEntity> parse(String url) throws IOException {
        Document document = Jsoup.connect(url).get();
        if (document != null) {
            return readAllFeeds(document);
        }

        return null;
    }

    public List<FeedEntity> readAllFeeds(Document document) {
        List<FeedEntity> feeds = new ArrayList<FeedEntity>();
        Elements items = document.select(ITEM);
        for (Element item : items) {
            Document innerDocument = Jsoup.parse(item.outerHtml(), "", Parser.xmlParser());
            feeds.add(readFeed(innerDocument));
        }
        return feeds;
    }

    public FeedEntity readFeed(Document document) {
        return new FeedEntity.Builder()
                .title(readTitle(document))
                .url(readLink(document))
                .pubDate(readPubDate(document))
                .description(readDescription(document))
                .smallImageUrl(readSmallImageUrl(document))
                .build();
    }

    public String readTitle(Document document) {
        Elements elements = document.select(TITLE);
        return elements.text();
    }

    public String readLink(Document document) {
        // <link> tag does not have a </link>, use nextSibling to extract url
        Node node = document.select(LINK).first().nextSibling();
        return node.toString();
    }

    public String readPubDate(Document document) {
        Elements elements = document.select(PUB_DATE);
        return elements.text();
    }

    public String readDescription(Document document) {
        Elements elements = document.select(DESCRIPTION);
        return elements.text();
    }

    public String readSmallImageUrl(Document document) {
        Elements elements = document.select(MEDIA_THUMBNAIL);
        return elements.attr(URL);
    }
}

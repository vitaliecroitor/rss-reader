package com.vcroitor.rssreader.parser;

import com.vcroitor.rssreader.database.model.FeedEntity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class TechCrunchHtmlParser {

    private static final String CLASS_MAIN = ".l-main";
    private static final String P_TAG = "p";
    private static final String NEW_LINE = "\n";
    private static final String ARTICLE_ENTRY = ".article-entry";
    private static final String IMG_TAG = "img";
    private static final String SRC_ATTR = "src";

    public FeedEntity parse(String url) throws IOException {
        Document document = Jsoup.connect(url).get();
        return new FeedEntity.Builder()
                .article(readArticle(document))
                .bigImageUrl(readImage(document))
                .build();
    }

    private String readArticle(Document document) {
        Elements articleWrapper = document.select(CLASS_MAIN);

        if (articleWrapper.size() > 0) {
            Elements elements = articleWrapper.get(0).getElementsByTag(P_TAG);
            StringBuilder articleBuilder = new StringBuilder();

            for (Element element : elements) {
                articleBuilder.append(element.text()).append(NEW_LINE);
            }

            return articleBuilder.toString();
        }

        return null;
    }

    private String readImage(Document document) {
        Elements articleWrapper = document.select(ARTICLE_ENTRY);
        if (articleWrapper.size() > 0) {
            Element image = articleWrapper.get(0).getElementsByTag(IMG_TAG).get(0);
            if (image != null) {
                return image.attr(SRC_ATTR);
            }
        }
        return null;
    }
}

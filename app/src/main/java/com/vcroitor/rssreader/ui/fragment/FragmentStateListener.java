package com.vcroitor.rssreader.ui.fragment;

import android.app.Fragment;

/**
 * Created by vitalie on 27/09/14.
 */
public interface FragmentStateListener {
    public void onBack(Fragment fragment);
}

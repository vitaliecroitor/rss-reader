package com.vcroitor.rssreader.ui.fragment;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;
import com.vcroitor.rssreader.R;
import com.vcroitor.rssreader.application.FeedApplication;
import com.vcroitor.rssreader.database.FeedContract;
import com.vcroitor.rssreader.database.model.FeedEntity;
import com.vcroitor.rssreader.networking.NetworkManager;
import com.vcroitor.rssreader.networking.VolleySingleton;
import com.vcroitor.rssreader.parser.TechCrunchHtmlParser;

import java.io.IOException;
import java.util.Arrays;

import static android.widget.Toast.LENGTH_SHORT;
import static com.vcroitor.rssreader.application.FeedApplication.getInstance;
import static com.vcroitor.rssreader.database.FeedContract.Feed.ARTICLE;
import static com.vcroitor.rssreader.database.FeedContract.Feed.BIG_IMAGE_URL;
import static com.vcroitor.rssreader.database.FeedContract.Feed.CONTENT_URI;
import static com.vcroitor.rssreader.database.FeedContract.Feed.PUB_DATE;
import static com.vcroitor.rssreader.database.FeedContract.Feed.TITLE;

public class FeedDetailsFragment extends Fragment {

    private static final String ARG_SELECTED_ID = "selected_id";
    private static final String ARG_URL = "url";
    private static final String ARG_PROGRESS_SHOW = "show_progress";
    private static final String TAG = FeedDetailsFragment.class.getName();

    private long selectedItemId = 0;
    private String feedUrl;
    private TextView content;
    private TextView title;
    private TextView date;
    private NetworkImageView contentImage;
    private static ProgressDialog progressDialog;
    private static boolean isShowingProgress = false;
    private FeedEntity feed;

    public static FeedDetailsFragment newInstance(long selectedItemId, String url) {
        FeedDetailsFragment fragment = new FeedDetailsFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_SELECTED_ID, selectedItemId);
        args.putString(ARG_URL, url);
        fragment.setArguments(args);
        return fragment;
    }

    public FeedDetailsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            selectedItemId = getArguments().getLong(ARG_SELECTED_ID);
            feedUrl = getArguments().getString(ARG_URL);
        }
        retrieveFeed();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(ARG_URL, feedUrl);
        outState.putLong(ARG_SELECTED_ID, selectedItemId);
        outState.putBoolean(ARG_PROGRESS_SHOW, isShowingProgress);
    }

    private void downloadFeed() {
        if (NetworkManager.isNetworkAvailable()) {
            showProgressDialog();
            DownloadHtmlTask task = new DownloadHtmlTask();
            task.execute(feedUrl);
        } else {
            Toast.makeText(getActivity(), getString(R.string.no_network), LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        createProgressDialog();
        if (savedInstanceState != null) {
            feedUrl = savedInstanceState.getString(ARG_URL);
            selectedItemId = savedInstanceState.getLong(ARG_SELECTED_ID);
            isShowingProgress = savedInstanceState.getBoolean(ARG_PROGRESS_SHOW);
            if (isShowingProgress) {
                showProgressDialog();
            }
        } else {
            downloadFeed();
        }

        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {
        View view = inflater.inflate(R.layout.fragment_feed_details, container, false);
        initViews(view);
        updateViews();
        return view;
    }

    private void initViews(View view) {
        content = (TextView) view.findViewById(R.id.tv_content);
        title = (TextView) view.findViewById(R.id.tv_title);
        date = (TextView) view.findViewById(R.id.tv_date);
        contentImage = (NetworkImageView) view.findViewById(R.id.iv_content_image);
    }

    private class DownloadHtmlTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            TechCrunchHtmlParser parser = new TechCrunchHtmlParser();
            try {
                FeedEntity feed = parser.parse(params[0]);
                updateFeed(feed);
                retrieveFeed();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage(), e);
            }
            return null;
        }

        private void updateFeed(FeedEntity feed) {
            String where = FeedContract.Feed._ID + "=?";
            String[] args = new String[]{String.valueOf(selectedItemId)};
            ContentValues values = new ContentValues();

            if (feed.article != null) {
                values.put(ARTICLE, feed.article);
            }
            if (feed.bigImageUrl != null) {
                values.put(FeedContract.Feed.BIG_IMAGE_URL, feed.bigImageUrl);
            }

            getInstance().getContentResolver().update(CONTENT_URI, values, where, args);
        }

        @Override
        protected void onPostExecute(Void param) {
            super.onPostExecute(param);
            updateViews();
            hideProgressDialog();
        }
    }

    private void retrieveFeed() {
        String[] projection = new String[]{ARTICLE, TITLE, PUB_DATE, BIG_IMAGE_URL};
        Uri uri = Uri.parse(CONTENT_URI.toString() + "/" + selectedItemId);
        Cursor cursor = FeedApplication.getInstance().getContentResolver()
                .query(uri, projection, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            Log.i(TAG, Arrays.toString(cursor.getColumnNames()));
            feed = new FeedEntity.Builder()
                    .article(cursor.getString(cursor.getColumnIndex(ARTICLE)))
                    .title(cursor.getString(cursor.getColumnIndex(TITLE)))
                    .pubDate(cursor.getString(cursor.getColumnIndex(PUB_DATE)))
                    .bigImageUrl(cursor.getString(cursor.getColumnIndex(BIG_IMAGE_URL)))
                    .build();
        }
    }

    private void updateViews() {
        content.setText(feed.article);
        title.setText(feed.title);
        date.setText(feed.pubDate);
        showImage(feed.bigImageUrl);
    }

    private void showImage(String url) {
        contentImage.setErrorImageResId(R.drawable.ic_launcher);
        contentImage.setDefaultImageResId(R.drawable.ic_launcher);
        contentImage.setImageUrl(url, VolleySingleton.getInstance().getImageLoader());
    }

    public void createProgressDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void showProgressDialog() {
        if (progressDialog != null) {
            isShowingProgress = true;
            progressDialog.show();
        }
    }

    public void hideProgressDialog() {
        if (progressDialog != null) {
            isShowingProgress = false;
            progressDialog.dismiss();
        }
    }
}

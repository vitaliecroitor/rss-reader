package com.vcroitor.rssreader.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.vcroitor.rssreader.R;
import com.vcroitor.rssreader.networking.VolleySingleton;

import static com.vcroitor.rssreader.database.FeedContract.Feed.PUB_DATE;
import static com.vcroitor.rssreader.database.FeedContract.Feed.SMALL_IMAGE_URL;
import static com.vcroitor.rssreader.database.FeedContract.Feed.TITLE;

public class FeedItemCursorAdapter extends CursorAdapter {

    private ViewHolder holder;

    public FeedItemCursorAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.feed_list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        holder = (ViewHolder) view.getTag();
        if (holder == null) {
            holder = new ViewHolder();
            holder.contentImage = (NetworkImageView) view.findViewById(R.id.iv_content_image);
            holder.title = (TextView) view.findViewById(R.id.tv_title);
            holder.date = (TextView) view.findViewById(R.id.tv_date);

            view.setTag(holder);
        }
        showView(cursor);
    }

    private void showView(Cursor cursor) {
        showImage(cursor.getString(cursor.getColumnIndex(SMALL_IMAGE_URL)));
        holder.title.setText(cursor.getString(cursor.getColumnIndex(TITLE)));
        holder.date.setText(cursor.getString(cursor.getColumnIndex(PUB_DATE)));
    }

    private void showImage(String imageUrl) {
        holder.contentImage.setDefaultImageResId(R.drawable.ic_launcher);
        holder.contentImage.setErrorImageResId(R.drawable.ic_launcher);
        holder.contentImage.setImageUrl(imageUrl, VolleySingleton.getInstance().getImageLoader());
    }


    private final class ViewHolder {
        private NetworkImageView contentImage;
        private TextView title;
        private TextView date;
    }
}

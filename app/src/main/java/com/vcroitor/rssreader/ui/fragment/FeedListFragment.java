package com.vcroitor.rssreader.ui.fragment;

import android.app.Activity;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Loader;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.vcroitor.rssreader.R;
import com.vcroitor.rssreader.application.FeedApplication;
import com.vcroitor.rssreader.database.model.FeedEntity;
import com.vcroitor.rssreader.networking.NetworkManager;
import com.vcroitor.rssreader.parser.TechCrunchXmlFeedParser;
import com.vcroitor.rssreader.ui.adapter.FeedItemCursorAdapter;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.List;

import static android.widget.Toast.LENGTH_SHORT;
import static com.vcroitor.rssreader.database.FeedContract.Feed.CONTENT_URI;
import static com.vcroitor.rssreader.database.FeedContract.Feed.DESCRIPTION;
import static com.vcroitor.rssreader.database.FeedContract.Feed.PUB_DATE;
import static com.vcroitor.rssreader.database.FeedContract.Feed.SMALL_IMAGE_URL;
import static com.vcroitor.rssreader.database.FeedContract.Feed.TITLE;
import static com.vcroitor.rssreader.database.FeedContract.Feed.URL;

public class FeedListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = FeedListFragment.class.getName();
    private static final String ARG_PROGRESS_SHOW = "show_progress";
    private FeedItemCursorAdapter adapter;
    private FragmentStateListener listener;
    private static final String FEED_URL = "http://feeds.feedburner.com/TechCrunch";
    private static ProgressDialog progressDialog;
    private static boolean isShowingProgress = false;

    public static FeedListFragment newInstance() {
        return new FeedListFragment();
    }

    public FeedListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        adapter = new FeedItemCursorAdapter(getActivity(), null, 0);
    }

    @Override
    public void onListItemClick(ListView list, View v, int position, long id) {
        super.onListItemClick(list, v, position, id);
        Cursor cursor = adapter.getCursor();
        if (cursor != null && cursor.moveToPosition(position)) {
            String url = cursor.getString(cursor.getColumnIndex(URL));
            listener.onBack(FeedDetailsFragment.newInstance(id, url));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_refresh: {
                refreshList();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void refreshList() {
        if (NetworkManager.isNetworkAvailable()) {
            DownloadXmlTask task = new DownloadXmlTask();
            task.execute(FEED_URL);
            showProgressDialog();
        } else {
            Toast.makeText(getActivity(), getString(R.string.no_network), LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        createProgressDialog();

        if (savedInstanceState != null) {
            isShowingProgress = savedInstanceState.getBoolean(ARG_PROGRESS_SHOW);
            if (isShowingProgress) {
                showProgressDialog();
            }
        }

        getActivity().getActionBar().setDisplayHomeAsUpEnabled(false);
        setListAdapter(adapter);
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(ARG_PROGRESS_SHOW, isShowingProgress);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {
        return inflater.inflate(R.layout.list_layout, container, false);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), CONTENT_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor newCursor) {
        adapter.swapCursor(newCursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (FragmentStateListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement FragmentStateListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    private class DownloadXmlTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            try {
                List<FeedEntity> feeds = readFeedList(params[0]);
                saveToDatabase(feeds);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }
            return null;
        }

        private List<FeedEntity> readFeedList(String feedUrl) throws IOException, XmlPullParserException {
            return new TechCrunchXmlFeedParser().parse(feedUrl);
        }

        private void saveToDatabase(List<FeedEntity> feeds) throws RemoteException, OperationApplicationException {
            if (feeds == null) {
                return;
            }
            // do not insert in transaction because some may fail
            for (FeedEntity feed : feeds) {
                ContentValues values = new ContentValues();
                values.put(PUB_DATE, feed.pubDate);
                values.put(DESCRIPTION, feed.description);
                values.put(TITLE, feed.title);
                values.put(URL, feed.url);
                values.put(SMALL_IMAGE_URL, feed.smallImageUrl);

                FeedApplication.getInstance().getContentResolver().insert(CONTENT_URI, values);
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgressDialog();
        }
    }

    private void createProgressDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void showProgressDialog() {
        if (progressDialog != null) {
            isShowingProgress = true;
            progressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            isShowingProgress = false;
            progressDialog.dismiss();
        }
    }
}

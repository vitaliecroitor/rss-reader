package com.vcroitor.rssreader.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.vcroitor.rssreader.database.FeedContract;
import com.vcroitor.rssreader.database.FeedDatabaseHelper;

import java.util.Arrays;
import java.util.HashSet;

import static android.provider.BaseColumns._ID;
import static com.vcroitor.rssreader.database.FeedContract.Feed.AUTHORITY;
import static com.vcroitor.rssreader.database.FeedContract.Feed.BASE_PATH;
import static com.vcroitor.rssreader.database.FeedContract.Feed.TABLE_NAME;
import static com.vcroitor.rssreader.database.FeedContract.Feed.getColumns;

public class FeedContentProvider extends ContentProvider {

    private static final int FEEDS = 10;
    private static final int FEED_ID = 20;

    private static UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sUriMatcher.addURI(AUTHORITY, BASE_PATH, FEEDS);
        sUriMatcher.addURI(AUTHORITY, BASE_PATH + "/#", FEED_ID);
    }

    private FeedDatabaseHelper databaseHelper;

    @Override
    public boolean onCreate() {
        databaseHelper = new FeedDatabaseHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        checkColumns(projection);
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        queryBuilder.setTables(TABLE_NAME);

        int uriType = sUriMatcher.match(uri);
        switch (uriType) {
            case FEEDS:
                break;
            case FEED_ID:
                queryBuilder.appendWhere(_ID + "=" + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        Cursor cursor = queryBuilder.query(database, projection, selection,
                selectionArgs, null, null, sortOrder);

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        int uriType = sUriMatcher.match(uri);

        switch (uriType) {
            case FEEDS: {
                return FeedContract.Feed.CONTENT_TYPE;
            }
            case FEED_ID: {
                return FeedContract.Feed.CONTENT_ITEM_TYPE;
            }
        }

        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sUriMatcher.match(uri);
        SQLiteDatabase database = databaseHelper.getWritableDatabase();

        long id = 0;
        switch (uriType) {
            case FEEDS:
                id = database.insert(TABLE_NAME, null, values);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return Uri.parse(BASE_PATH + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sUriMatcher.match(uri);
        SQLiteDatabase database = databaseHelper.getWritableDatabase();

        int rowsDeleted = 0;
        switch (uriType) {
            case FEEDS:
                rowsDeleted = database.delete(TABLE_NAME, selection, selectionArgs);
                break;
            case FEED_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = database.delete(TABLE_NAME, _ID + "=" + id, null);
                } else {
                    rowsDeleted = database.delete(TABLE_NAME, _ID + "=" + id + " and " + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int uriType = sUriMatcher.match(uri);
        SQLiteDatabase database = databaseHelper.getWritableDatabase();

        int rowsUpdated = 0;
        switch (uriType) {
            case FEEDS:
                rowsUpdated = database.update(TABLE_NAME, values, selection, selectionArgs);
                break;
            case FEED_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = database.update(TABLE_NAME, values, _ID + "=" + id, null);
                } else {
                    rowsUpdated = database.update(TABLE_NAME, values,
                            _ID + "=" + id + " and " + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return rowsUpdated;
    }

    private void checkColumns(String[] projection) {
        if (projection != null) {
            HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
            HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(getColumns()));
            if (!availableColumns.containsAll(requestedColumns)) {
                throw new IllegalArgumentException("Unknown columns in projection");
            }
        }
    }
}
